import groovy.json.JsonSlurper
def module = "xxx.ui.apps"
def username = ""
def password = ""
def Jenkins = this.binding.getVariable("jenkins")
def loader = Jenkins.instance.pluginManager.getPlugin("credentials").classLoader
def crprov = loader.loadClass("com.cloudbees.plugins.credentials.CredentialsProvider", true)
def crusnm = loader.loadClass("com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials", true)
def credentials = crprov.lookupCredentials(crusnm, Jenkins.instance, null, null)
credentials.each { cred -> 
    if(cred.getId() == '16xxxxxb1' ) {
        username = cred.getUsername()
        password = cred.getPassword()
    }
}

def url =  "https://artifactory.com/artifactory/api/search/pattern?pattern=ads-snapshot:com/bbt/aem/${module}/*/${module}*.zip"
def authString = "${username}:${password}".getBytes().encodeBase64().toString()
def conn = url.toURL().openConnection()
conn.setRequestProperty("Authorization", "Basic ${authString}")
List<String> versions = new ArrayList<String>()
def json = conn.inputStream.text
def slurper = new JsonSlurper()
def valueJson = slurper.parseText(json)
for(file in valueJson.files) {
    def version = (file =~ /(\d\.\d\.\d-\d{8}\.\d{6}-\d{1,2})/)[0][1]
    versions.add(version)
}
return versions.sort().reverse()
