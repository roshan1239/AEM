def GIT_TAG
node('linux') {
  stage("Checkout") {
    checkout scm
  }
  stage("Build and Test") {
    def mvnHome = tool 'Maven 3.3.x'
    echo "mvnHome = ${mvnHome}"
    def target = "package"
    if (env.BRANCH_NAME =~ /master/) {
      target = "deploy"
      properties([[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '2', artifactNumToKeepStr: '2', daysToKeepStr: '5', numToKeepStr: '10']], gitLabConnection('GitLabProd'), pipelineTriggers([])])
    }

    withCredentials([usernamePassword(credentialsId: '16476xxxxfff0xx', passwordVariable: 'ARTIFACTORY_PASSWORD', usernameVariable: 'ARTIFACTORY_USERNAME'), 
usernamePassword(credentialsId: '1647xxxxxxfff02b1', passwordVariable: 'MAVEN.PASSWORD', usernameVariable: 'MAVEN.USERNAME')]) {

      configFileProvider(
          [configFile(fileId: 'org.jenkinsci.plugins.configfiles.maven.GlobalMavenSettingsConfig1429132350345', variable: 'MAVEN_SETTINGS')]) {
          sh "${mvnHome}/bin/mvn -s $MAVEN_SETTINGS clean ${target}"
          // Curl to get artifactory-assigned version string
          def pom = readMavenPom file: "pom.xml";
          for (String module : pom.modules){
			  if(module != "core" && module != "ui.content"){
				GIT_TAG = sh(script: "curl -u \"${ARTIFACTORY_USERNAME}:${ARTIFACTORY_PASSWORD}\" \"https://artifactory.com/artifactory/api/search/latestVersion?g=${pom.groupId}&a=${pom.artifactId}&v=${pom.version}&repos=ads-snapshot\"", returnStdout: true);
				print("Latest GIT Version: " + GIT_TAG);
			  }
		  }
      }
    }
  }
  stage("Dev branch = Archive/Master = Deploy Artifacts ") {
      archive(includes: "**/target/*.zip")
     // step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])
  }

    def parent_artifactId;
     def version;
     def groupId;
     def modules;

     if (env.BRANCH_NAME =~ /develop/){
       properties([[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '1', artifactNumToKeepStr: '1', daysToKeepStr: '1', numToKeepStr: '10']], gitLabConnection('GitLabProd'), pipelineTriggers([[$class: 'TimerTrigger', spec: 'H 6,15 * * *']])])
       stage('Read Pom') {
       def pom = readMavenPom(file: 'pom.xml');
       modules = pom.modules;
       parent_artifactId = pom.artifactId;
       version = pom.version;
       groupId = pom.groupId.replaceAll("[.]", "/");
      }

     print "Modules: ${modules}";
     for (String module : modules) {
      print("Reading pom: ${module}/pom.xml");
      pom = readMavenPom(file: "${module}/pom.xml");
      print("Found Module: ${pom.artifactId}, packaging is: ${pom.packaging}");
      def artifactId = pom.artifactId;
      if (pom.packaging == "content-package" && !(artifactId =~ /ui.content$/)) {
        def filename = "${module}/target/${artifactId}-${version}.zip"
         upload(parent_artifactId, artifactId, version, filename);
      }
    }
    }
}

def jsonCurl(String url, boolean post = false, String filename = "", String fileNameWithoutPath = "", boolean install = false) {
  def request = "curl -u \"${AEM_USERNAME}:${AEM_PASSWORD}\" "
  if (filename != "") {
    request += "-F force=true -F \"package=@${filename}\" "
  }
   if (fileNameWithoutPath != "") {
     request += "-F force=true -F \"file=${fileNameWithoutPath}\" "
   }
  if (post) {
    request += "-X POST "
  }
  if (install) {
    request += "-F install=true "
  }
  request += url
  def response = sh(script: "$request", returnStdout: true)
  def json = readJSON(text: response)
  if (json["success"] == false) {
    error(message: json["msg"])
  }
}

def upload(String parent_artifactId, String artifactId, String version, String filename) {
  stage("Upload and Install ${artifactId} to Dev Author Server") {
    withCredentials([usernamePassword(credentialsId: 'AEM-DEV-AUTHOR-SERVER', usernameVariable: 'AEM_USERNAME', passwordVariable: 'AEM_PASSWORD')]) {
      fileNameWithoutPath = filename.split("/").last()
      sh "curl -u \"${AEM_USERNAME}:${AEM_PASSWORD}\" -F file=\"@${filename}\" -F name=\"${fileNameWithoutPath}\" -F force=true -F install=true http://xxxxxx:4502/crx/packmgr/service.jsp ";
         
    }
  }

  stage("Upload and Install ${artifactId} to Dev Publish Server") {
    withCredentials([usernamePassword(credentialsId: 'AEM-DEV-PUBLISH-SERVER', usernameVariable: 'AEM_USERNAME', passwordVariable: 'AEM_PASSWORD')]) {    
      sh "curl -u \"${AEM_USERNAME}:${AEM_PASSWORD}\" -F file=\"@${filename}\" -F name=\"${fileNameWithoutPath}\" -F force=true -F install=true http://xxxx:4503/crx/packmgr/service.jsp ";
    }
  }
}
if (env.BRANCH_NAME =~ /master/) {
stage ('Tag Build') {
node ('chefdk')  {
    checkout scm
    sh """
    git config --global user.email "bcb-admins@xx.com"
    git config --global user.name "Jenkins Automation"
    git tag -a $GIT_TAG -m "Version created by Jenkins Build"
    git push origin $GIT_TAG
    """
    }
  }
}
