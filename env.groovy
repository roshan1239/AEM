def AUTHOR_CREDENTIALS_ID
def PUBLISH_CREDENTIALS_ID
AUTHOR_CREDENTIALS_ID="AEM-DEV-AUTHOR-SERVER"
PUBLISH_CREDENTIALS_ID="AEM-DEV-AUTHOR-SERVER"

node('linux') {
    echo "Deploying ${module} version: ${Version} to ${DeployEnvironment}"
    currentBuild.displayName = "${Version} -> ${DeployEnvironment}";
    def tmp = getFiles(module, Version)
    def resp = readJSON(text: tmp)
    def repoUri = resp.repoUri
    print("repoURI: ${repoUri}")
    def files = resp.files
    print("Files: ${files}")

    for (def i=0; i < files.size();i++) {
        def filename = files[i]
        download(repoUri, filename)
        print("Downloading ${filename}")
        
		switch (DeployEnvironment) {
			case "DEV":
				upload(module, filename, AUTHOR_CREDENTIALS_ID, '1xxxxx36:4502') //AUTHOR SERVER
				upload(module, filename, PUBLISH_CREDENTIALS_ID, '1xxxxx2:4503')  //PUBLISH SERVER
				break;
			   
			case "SIT":
				upload(module, filename, AUTHOR_CREDENTIALS_ID, 'xxx:4502')	//AUTHOR SERVER
				upload(module, filename, PUBLISH_CREDENTIALS_ID, 'xxx1:4503')  //PUBLISH SERVER
				upload(module, filename, PUBLISH_CREDENTIALS_ID, 'xxxx:4503')  //PUBLISH SERVER
				break;
				
			case "UAT":
				upload(module, filename, PUBLISH_CREDENTIALS_ID, '1xxx3:4503')  //PUBLISH SERVER
				upload(module, filename, PUBLISH_CREDENTIALS_ID, '1xxx03:4503')	//PUBLISH SERVER
				upload(module, filename, PUBLISH_CREDENTIALS_ID, '1xxx04:4503')	//PUBLISH SERVER
				break;
				
			case "PROD_AUTHOR":
				upload(module, filename, AUTHOR_CREDENTIALS_ID, '1xxxx1:4502')	//AUTHOR SERVER
				break;
				
			case "PROD_PUBLISHER":
				upload(module, filename, PUBLISH_CREDENTIALS_ID, '1xxxx9:4503')  //PUBLISH SERVER
				upload(module, filename, PUBLISH_CREDENTIALS_ID, '1xxx2:4503')	//PUBLISH SERVER
				upload(module, filename, PUBLISH_CREDENTIALS_ID, '1xxxx3:4503')	//PUBLISH SERVER
				break;
		}
	}
}


String getFiles(String module, String version) {
     def buildNum = (version =~ /(\d\.\d\.\d-\d{8}\.\d{6}-\d{1,2})/)[0][1]

      withCredentials([usernamePassword(credentialsId: 'xxxxxx', passwordVariable: 'pass', usernameVariable: 'user')]) {
           return sh(script: "curl -u${user}:${pass} https://artifactory.com/artifactory/api/search/pattern?pattern=ads-snapshot:com/bbt/aem/${module}/*/${module}*-${buildNum}.zip", returnStdout: true)
      }
}
     def download(String repoUri, String filename) {
         def artifact = (filename =~ /([^\/]*)-(\d\.\d\.\d-\d{8}\.\d{6}-\d{1,2})/)[0][1];
        stage("Download ${artifact}") {

        withCredentials([usernamePassword(credentialsId: '1xxxxx6xxxxx2b1', passwordVariable: 'pass', usernameVariable: 'user')]) {

            sh "curl -O  -u${user}:${pass} ${repoUri}/${filename}"
            pom = (filename =~ /(.*).zip/)[0][1] + ".pom"
            sh "curl -O  -u${user}:${pass} ${repoUri}/${pom}"
        }

      }
    }

    def upload(String module, String filename, String credentialsId, String server) {
        def file = (filename =~ ".*/([^/]*.zip)")[0][1];
        def artifact = (filename =~ /([^\/]*)-(\d\.\d\.\d-\d{8}\.\d{6}-\d{1,2}).zip/)[0][1];
        withCredentials([usernamePassword(credentialsId: credentialsId, usernameVariable: 'AEM_USERNAME', passwordVariable: 'AEM_PASSWORD')]) {
          stage("Upload and Install to ${DeployEnvironment} ${server}") {
             pom = readMavenPom file: (file =~ /(.*).zip/)[0][1] + ".pom"
             
             packageFilename = pom.getParent().getArtifactId() + "/" + module + "-" + pom.getParent().getVersion() + ".zip";
             fileNameWithoutPath = filename.split("/").last()
             snapshotVersion =  filename.split('/')[-2]
             artifactName = "/bbt-platform-ui-" + snapshotVersion + ".zip"
                sh "curl -u \"${AEM_USERNAME}:${AEM_PASSWORD}\" -F file=\"@${file}\" -F name=\"${fileNameWithoutPath}\" -F force=true -F install=true http://${server}/crx/packmgr/service.jsp";
          }
        }
    }
   

  def jsonCurl(String url, boolean post = false, String filename = "") {
    def request = "curl -vu \"${AEM_USERNAME}:${AEM_PASSWORD}\" "
  if (filename != "") {
    request += "-F force=true -F \"package=@${filename}\" "
  }
  if (post) {
    request += "-X POST "
  }
  request += url
  def response = sh(script: "$request", returnStdout: true)
  def json = readJSON(text: response)
  if (json["success"] == false) {
    error(message: json["msg"])
  } else {
      print(json["msg"])
  }
}
